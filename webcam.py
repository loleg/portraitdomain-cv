import sys
import cv2
import qrcode
from cascade import Cascade

if len(sys.argv) < 1:
    print "Usage: python webcam.py [DEBUG]"
    print "images in ./faces and codes in ./qr expected"
    print "press q to exit the program"
    sys.exit()

SOURCES = {
    'images':   'faces',
    'metadata': 'texts',
    'qrcodes':  'qr'
}

DEBUG = False
if len(sys.argv) > 1:
    DEBUG = sys.argv[1] == 'DEBUG'

FULLSCREEN = not DEBUG

# Subfolder containing images
cascade = Cascade(SOURCES)
print "%d faces loaded" % cascade.numportraits

# Initialise OpenCV capture
videoCapture = cv2.VideoCapture(0)
print "Video capture ready"

# Initialise OpenCV window
if FULLSCREEN:
    cv2.namedWindow("Portrait Id", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("Portrait Id", cv2.WND_PROP_FULLSCREEN, 1)
else:
    cv2.namedWindow("Portrait Id")

print "OpenCV version: %s" % (cv2.__version__)
print "Press q to exit ..."

while videoCapture.isOpened():
    # Capture frame-by-frame
    ret, frame = videoCapture.read()
    frame = cv2.flip(frame, 1)

    # Process the frame
    output = cascade.process_frame(frame)

    # Display the resulting frame
    cv2.imshow('Portrait Id', output)

    # Wait for the magic key
    keypress = cv2.waitKey(1) & 0xFF
    if keypress == ord('q'):
        break
    elif keypress == ord('n'):
        cascade.go_next()

# When everything is done, release the capture
videoCapture.release()
cv2.destroyAllWindows()
