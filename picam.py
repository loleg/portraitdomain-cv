from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import sys
import cv2

from cascade import Cascade

if len(sys.argv) < 2: 
    print "Usage: python picam.py <log token> [DEBUG]"
    print "images in ./faces and codes in ./qr expected"
    print "press q to exit the program"
    sys.exit()

SOURCES = {
    'images':   'faces',
    'metadata': 'texts',
    'qrcodes':  'qr'
}

# Debug mode
DEBUG = False
LOGENTRIES_TOKEN = None
if len(sys.argv) > 1:
	DEBUG = sys.argv[-1] == 'DEBUG'
	if sys.argv[1] != 'DEBUG':
		LOGENTRIES_TOKEN = sys.argv[1]

# Configuration options
FULLSCREEN = not DEBUG
#if DEBUG:
RESOLUTION = (640, 480)
#else:
#	RESOLUTION = (480, 270)

# Subfolder containing images
cascade = Cascade(SOURCES, LOGENTRIES_TOKEN)
print "%d faces loaded" % cascade.numportraits

# Initialise Raspberry Pi camera
camera = PiCamera()
camera.resolution = RESOLUTION
#camera.framerate = 10
camera.vflip = True
#camera.hflip = True
#camera.color_effects = (128, 128)
# set up stream buffer
rawCapture = PiRGBArray(camera, size=RESOLUTION)
# allow camera to warm up
time.sleep(0.1)
print "PiCamera ready"

# Initialise OpenCV window
if FULLSCREEN:
	cv2.namedWindow("Portrait Id", cv2.WND_PROP_FULLSCREEN)
	cv2.setWindowProperty("Portrait Id", cv2.WND_PROP_FULLSCREEN, cv2.cv.CV_WINDOW_FULLSCREEN)
else:
	cv2.namedWindow("Portrait Id")

print "OpenCV version: %s" % (cv2.__version__)
print "Press q to exit ..."

cc = 0

# Capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# as raw NumPy array
	output = frame.array.copy()
	
	# Process every other frame
	cc = cc + 1
	output = cascade.process_frame(output, cc % 2 == 0)
    
	# show the frame
	cv2.imshow("Portrait Id", output)

	# clear stream for next frame
	rawCapture.truncate(0)

	# Wait for the magic key
	keypress = cv2.waitKey(1) & 0xFF
	if keypress == ord('q'):
		break
	elif keypress == ord('n'):
		cascade.go_next()
		
# When everything is done, release the capture
camera.close()
cv2.destroyAllWindows()
