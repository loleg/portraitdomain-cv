import cv2
import sys
from os import listdir
from os.path import isfile, join

from cascade import Cascade
cascade = Cascade()

if len(sys.argv) < 3: 
    print "Usage: python cropper.py <source dir> <target dir>"
    sys.exit()

sourcePath = sys.argv[1]
targetPath = sys.argv[2]

print "OpenCV version: %s" % (cv2.__version__)

coll = [ f for f in listdir(sourcePath) 
            if isfile(join(sourcePath, f)) 
            and '.jpg' in f ]

count = 0
for imgfile in coll:
    print "Cropping %s..." % imgfile
    fn = join(sourcePath, imgfile)
    # Run the cascade
    thumb = cascade.get_face_crop(fn, True, False)
    if thumb is None:
        print "Face not detected, skipping!"
    else:
        cv2.imwrite(join(targetPath, imgfile), thumb)
        count = count + 1

print "Done (%d files)." % count