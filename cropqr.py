import cv2
import string
import sys
from os import listdir
from os.path import isfile, join

import qrcode

from identifier import getShortText

if len(sys.argv) < 3: 
    print "Usage: python cropqr.py <source dir> <target dir>"
    sys.exit()

sourcePath = sys.argv[1]
targetPath = sys.argv[2]
BASE_URL = 'http://portraits.soda.camp/go/'

coll = [ f for f in listdir(sourcePath) 
            if isfile(join(sourcePath, f)) 
            and '.jpg' in f ]

count = 0
for imgfile in coll:
    fn = imgfile.decode('utf-8').split("_-_")[0].replace('_', ' ')
    sh = getShortText(fn, 16)
    url = BASE_URL + sh
    print "Linking %s to /%s" % (sh, fn)
    # Create code
    f = open(join(targetPath, sh + '.png'), 'wb')
    img = qrcode.make(url)
    img.save(f)
    f.close
    count = count + 1

print "Done (%d files)." % count