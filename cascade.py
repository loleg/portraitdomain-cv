import cv2
import os.path
import logging

from time import time
from random import shuffle
from identifier import (getIdentifier, getShortText)
#from logentries import LogentriesHandler
import logging

#logging.basicConfig(filename='cascade.log', level=logging.INFO, format='%(asctime)s %(message)s')

class Cascade:

    # Path to face classifier parameters
    cascPath = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascPath)
    maskImage = cv2.imread('mask.png')
    maskGray = cv2.cvtColor(maskImage, cv2.COLOR_BGR2GRAY)
    maskRet, maskAlpha = cv2.threshold(maskGray, 10, 255, cv2.THRESH_BINARY)

    # Time to elapse between portraits
    CYCLEAFTER = 10

    # Time after which start fade
    HALFTIME = CYCLEAFTER - 2
    CYCLESPEED = 0.3

    # Maximum transparency of masks
    FULLALPHA = 0.9

    # Increase box by a percentage
    PADDING = 0

    # Method of overlaying image
    MIXMODE_GLOW = True

    # Performance optimisation
    ADAPTIVE_SCALING = True

    # Show only one mask at a time
    ONE_FACE_MASKED = False

    # Show rectangle for detector debugging
    DEBUG_RECT = False

    # Characters of description to show
    MAX_LEN_DESCRIPTION = 50

    # Runtime parameters
    curportrait = 0
    cursequence = 0
    codexy = None
    lastfaces = []
    randsequence = []
    pastportraits = []
    log = None

    # Detection parameters
    min_face_size = 50
    max_face_size = 250

    def __init__(self, pathConfig, logToken=None):
        if pathConfig is None: return
        p, m = self.load_faces([ pathConfig['images'], pathConfig['metadata'] ])
        self.portraits = p
        self.pormetas = m
        #self.porqrcode = self.load_qrcodes(pathConfig['qrcodes'])
        self.numportraits = len(p)
        self.init_sequence()
        self.timestart = time()
        self.sessionstart = time()
        #self.initLog(logToken)
        self.go_next()

    def initLog(self, logToken=None):
        if logToken is None: return
        self.log = logging.getLogger('logentries')
        self.log.setLevel(logging.INFO)
        lehandler = LogentriesHandler(logToken)
        self.log.addHandler(lehandler)

    # Unicode special characters table, needed
    # due to lack of QT support in our OpenCV lib
    translations = (
        (u'\N{LATIN SMALL LETTER U WITH DIAERESIS}', u'ue'),
        (u'\N{LATIN SMALL LETTER O WITH DIAERESIS}', u'oe'),
        (u'\N{LATIN SMALL LETTER A WITH DIAERESIS}', u'ae'),
        (u'\N{LATIN SMALL LETTER E WITH ACUTE}', u'e'),
        (u'\N{LATIN SMALL LETTER C WITH CEDILLA}', u'c'),
    )

    def translateUTF(self, name):
        name = name.replace('_', ' ')
        for from_str, to_str in self.translations:
            name = name.replace(from_str, to_str)
        return name

    def trimWords(self, txt, length):
        txt = txt[:length]
        if length > len(txt): return txt
        for i in range(len(txt)-1, 0, -1):
            if txt[i] == " ":
                return txt[:i]
        return txt

    def load_faces(self, folders=[], andCrop=False):
        folderImage = folders[0]
        folderMeta = folders[1]
        portraits = []
        pormetas = []
        for fn in os.listdir(folderImage):
            path = os.path.join(folderImage, fn)
            pathmeta = os.path.join(folderMeta, fn.replace('tif.jpg', 'tif.txt'))
            # Load the portrait from the file
            portraits.append(self.get_face_crop(path, andCrop))
            # Load metadata
            if not os.path.isfile(pathmeta):
                myname = "Anonymous"
                if "CH-BAR" in fn:
                    myname = fn.split('_-_')[0].replace('_', ' ')
                pormetas.append([ myname, "", "" ])
                continue
            fmeta = file(pathmeta, 'r')
            metatxt = fmeta.read().decode('utf-8')
            metalines = metatxt.splitlines()
            if metalines[1].startswith('Personalien:') or metalines[1].startswith('Ort:'):
                descline = metalines[1].replace('Personalien:', '').replace('Ort:', '')
            else:
                descline = metalines[-1]
            descline = self.trimWords(self.translateUTF(descline), self.MAX_LEN_DESCRIPTION)
            fmeta.close()
            # Decode the name
            name = self.translateUTF(fn.decode('utf-8').split("_-_")[0])
            link = getShortText(name, 16)
            print "%s (%s): %s" % (name, link, descline)
            pormetas.append([ name, link, descline ])
        return portraits, pormetas

    def load_qrcodes(self, folder):
        images = {}
        for fn in os.listdir(folder):
            path = os.path.join(folder, fn)
            img = self.get_face_crop(path, False)
            # Decode and reformat names
            name = fn.split(".")[0]
            images[name] = img
        return images

    def get_face_crop(self, filename, andCrop, andGray=False):
        """ Load image, detect a face, return crop """
        image = cv2.imread(filename, 0)
        # Convert to gray if necessary
        if andGray:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if not andCrop:
            # Apply the mask
            #image = cv2.multiply(image, self.maskAlpha)
            return image
        faces = self.faceCascade.detectMultiScale(
                image,
                scaleFactor=1.1,
                minNeighbors=5,
                minSize=(50, 50),
                flags=cv2.CASCADE_SCALE_IMAGE
            )
        # TODO: more intelligently select which face
        for (x, y, w, h) in faces:
            # Draw a rectangle for debugging
            #cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
            # Crop the first image and return it
            return image[y:y+h, x:x+w]

    def write_text(self, frame, text, bright=1, pinBottom=False):
        if len(self.lastfaces) == 0 or bright == 0: return
        # Center the x
        x = frame.shape[1] / 2 - len(text) * 5
        # Pin the y
        y = 30
        if pinBottom: y = frame.shape[0] - y
        cv2.putText(
            frame,
            text.encode('latin1'),
            (x, y), # x, y
            cv2.FONT_HERSHEY_PLAIN,
            1, # size
            (255*bright,255*bright,255*bright), # color
            1, # thickness of line
            #cv2.LINE_AA # options
        )

    def show_code(self, frame, ident, isVisible=False):
        if not ident in self.porqrcode: return
        imgsize = frame.shape[1] / 10
        # Calculate locations
        if self.codexy is None:
            self.codexy = [
                #[ 0, 0 ], # top left
                #[ frame.shape[1] - imgsize, 0 ], # top right
                #[ 0, frame.shape[0] - imgsize ], # bottom left
                [ frame.shape[1] - imgsize, frame.shape[0] - imgsize ], # bottom right
            ]
        # Show white box by default
        if not isVisible:
            for [ x, y ] in self.codexy:
                cv2.rectangle(frame, (x, y), (x+imgsize, y+imgsize), (255, 255, 255), cv2.cv.CV_FILLED)
            return
        # Find and resize QR code
        img = self.porqrcode[ident]
        if img.shape[0] != imgsize:
            img = cv2.resize(img, (imgsize, imgsize))
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            self.porqrcode[ident] = img
        # Apply image to frame
        for [ x, y ] in self.codexy:
            frame[ y:y+imgsize, x:x+imgsize ] = img

    def cycle_alpha(self):
        # Cycle portraits
        pcalpha = self.FULLALPHA
        halftime = self.HALFTIME
        timelapse = time() - self.timestart
        if timelapse < self.CYCLEAFTER - halftime:
            pcalpha = self.CYCLESPEED * timelapse
        if timelapse > halftime:
            pcalpha = self.FULLALPHA - (self.CYCLESPEED * (timelapse - halftime))
        if timelapse > self.CYCLEAFTER:
            self.go_next()
            pcalpha = 0
        return pcalpha

    def init_sequence(self):
        self.randsequence = range(0, self.numportraits-1)
        shuffle(self.randsequence)

    def go_next(self):
        self.cursequence = self.cursequence + 1
        if self.cursequence >= len(self.randsequence) - 1:
            self.cursequence = 0
        self.curportrait = self.randsequence[self.cursequence]
        # Add to history
        curid = str(self.curportrait + 20507)
        self.pastportraits.append(curid)
        # Reset fader
        self.timestart = time()
        self.sessionstart = time()

    def get_current_portrait(self):
        return self.portraits[self.curportrait]

    def get_current_meta(self):
        return self.pormetas[self.curportrait]

    def get_past_numbers(self):
        if len(self.pastportraits) < 1: return ""
        return self.pastportraits[len(self.pastportraits)-1]
        if len(self.pastportraits) < 3: return ""
        return "..%s %s*" % (
            " ".join(self.pastportraits[-3:-1]),
            self.pastportraits[len(self.pastportraits)-1]
        )

    def blip_to_log(self, t):
        if self.log is None: return
        name = self.get_current_meta()[0]
        self.log.info("'%s' was viewed for %ds" % (name, t))

    def process_frame(self, frame, withDetect=True):
        # Fetch animation parameters
        pcalpha = self.cycle_alpha()

        # Apply writing on the wall
        curmeta = self.get_current_meta()
        self.write_text(frame, curmeta[0], pcalpha)
        self.write_text(frame, curmeta[2], pcalpha, True)
        #self.write_text(frame, self.get_past_numbers(), pcalpha, True)

        # Detection code
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if not withDetect:
            faces = []
        else:
            faces = self.faceCascade.detectMultiScale(
                gray,
                scaleFactor=1.25,
                minNeighbors=4,
                # rejectLevels=1,
                # levelWeights=1,
                minSize=(self.min_face_size, self.min_face_size),
                maxSize=(self.max_face_size, self.max_face_size),
                flags=cv2.CASCADE_SCALE_IMAGE
            )

        # Freeze last known position
        if len(faces) == 0:
            faces = self.lastfaces
            if pcalpha < 0.2:
                self.lastfaces = faces = []
        else:
            self.timestart = time() - (self.CYCLEAFTER / 2)
            self.lastfaces = faces

        # Show qr code
        #self.show_code(frame, self.get_current_meta()[1], len(faces) > 0)

        # Register blip
        if len(faces) > 0 and pcalpha > 0:
            timesession = time() - self.sessionstart
            if timesession > 5:
                self.sessionstart = time()
                self.blip_to_log(timesession)

        # Apply mask to each face
        for (x, y, w, h) in faces:

            # Draw a rectangle around the face for testing
            if self.DEBUG_RECT:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                #continue

            # Expand a little
            pad = int(w * self.PADDING)
            if x > pad: x = x - pad
            if y > pad: y = y - pad
            if h < frame.shape[0] + pad: h = h + pad + pad
            if w < frame.shape[1] + pad: w = w + pad + pad

            # Select portrait, resize it
            pcgrayed = cv2.resize(self.get_current_portrait(), (w, h))
            # Apply the mask
            masksize = cv2.resize(self.maskImage, (w, h))
            #masksize = cv2.cvtColor(masksize, cv2.COLOR_GRAY2RGB)
            # Convert colors
            pcrgb = cv2.cvtColor(pcgrayed, cv2.COLOR_GRAY2RGB)

            # Overlay image file
            if y+pcrgb.shape[0] < frame.shape[0] and x+pcrgb.shape[1] < frame.shape[1]:
                gl = frame[ y:y+pcrgb.shape[0], x:x+pcrgb.shape[1] ]

                # Apply alpha
                if self.MIXMODE_GLOW:
                    pointalpha = pcalpha * masksize / 256
                    gl = ((1 - pointalpha) * gl) + (pcrgb * pointalpha)
                else:
                    gl = pcrgb

                frame[ y:y+pcrgb.shape[0], x:x+pcrgb.shape[1] ] = gl

            if self.ADAPTIVE_SCALING:
                self.min_face_size = int(w * 0.8)
                if self.min_face_size < 80: self.min_face_size = 80
                self.max_face_size = int(w * 1.4)
                if self.max_face_size < 350: self.max_face_size = 350

            if self.ONE_FACE_MASKED:
                break # one face only

        # Output result
        return frame
