# Portrait Id

This is a Computer Vision project demo developed for the [OpenGLAM.ch Hackathon](http://openglam.ch) in Berne, Switzerland on February 27-28, 2015. For background information see the [concept](https://docs.google.com/document/d/1qBTeD2aGTcyGEKOV28KBpriDiGb5BBiKpRs48SAEB3w/edit?usp=sharing).

## Installation

Instructions for Linux:

```
TBD
```

## Usage

1. Download some public domain portraits.

For example from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Category:18th-century_portrait_paintings_of_women_at_bust_length), using our [shell script](https://gist.github.com/loleg/0c43884452943eee7b25) or [Python script](https://bitbucket.org/tamberg/makeopendata/src/d42b1ba71983dda7098c77025bf2084270de4a35/2015/PictureThis/scraper.py?at=default) as you like.

Collect them in JPEG format in a subfolder (<source folder>).

2. Extract faces from using the image processing script:

Create an empty target folder first.

```
python cropper.py <source folder> <target folder>
```

3. Run the interactive program like this:

```
python webcam.py <target folder>
```

or on a Raspberry Pi:
```
python picam.py <target folder>
```

Press q to exit, look away or press n to advance to the next image.

## Background links

http://milq.github.io/install-opencv-ubuntu-debian/
http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
http://cbarker.net/opencv/
https://realpython.com/blog/python/face-detection-in-python-using-a-webcam/
http://www.pyimagesearch.com/2015/02/23/install-opencv-and-python-on-your-raspberry-pi-2-and-b/
http://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/
http://www.raspberrypi.org/facial-recognition-opencv-on-the-camera-board/
http://picamera.readthedocs.org/en/release-1.10/

## Training references

http://docs.opencv.org/modules/objdetect/doc/cascade_classification.html
http://docs.opencv.org/doc/user_guide/ug_traincascade.html
http://stackoverflow.com/questions/17184818/opencv-traincascade-for-lbp-training
http://note.sonots.com/SciSoftware/haartraining.html
http://www.irshadali.info/index.php/downloads
http://abhishek4273.com/2014/03/16/traincascade-and-car-detection-using-opencv/
http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_objdetect/py_face_detection/py_face_detection.html